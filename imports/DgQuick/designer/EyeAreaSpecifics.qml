import QtQuick 2.1
import HelperWidgets 2.0
import QtQuick.Layouts 1.0

Column {
    width: parent.width
    Section {
        width: parent.width
        caption: qsTr("General")

        SectionLayout {
            Label {
                text: qsTr("enabled")
                tooltip: qsTr("enabled")
            }
            SecondColumnLayout {
                CheckBox {
                    backendValue: backendValues.enabled
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("croppingEnabled")
                tooltip: qsTr("croppingEnabled")
            }
            SecondColumnLayout {
                CheckBox {
                    backendValue: backendValues.croppingEnabled
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("eyeFocusDwellTime")
                tooltip: qsTr("eyeFocusDwellTime")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.eyeFocusDwellTime
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("actionDwellTime")
                tooltip: qsTr("actionDwellTime")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.actionDwellTime
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("dragAndDropButtonName")
                tooltip: qsTr("dragAndDropButtonName")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.dragAndDropButtonName
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("dragAndDropEnabled")
                tooltip: qsTr("dragAndDropEnabled")
            }
            SecondColumnLayout {
                CheckBox {
                    backendValue: backendValues.dragAndDropEnabled
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("focusGroup")
                tooltip: qsTr("focusGroup")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.focusGroup
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("focusActionMode")
                tooltip: qsTr("focusActionMode: ActionModeDwell/ActionModeButton")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.focusActionMode
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("focusButtonName")
                tooltip: qsTr("focusButtonName")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.focusButtonName
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("scrollEnabled")
                tooltip: qsTr("scrollEnabled")
            }
            SecondColumnLayout {
                CheckBox {
                    backendValue: backendValues.scrollEnabled
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("zoomEnabled")
                tooltip: qsTr("zoomEnabled")
            }
            SecondColumnLayout {
                CheckBox {
                    backendValue: backendValues.zoomEnabled
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("horizontalMagnification")
                tooltip: qsTr("horizontalMagnification")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.horizontalMagnification
                }
                ExpandingSpacer {
                }
            }

            Label {
                text: qsTr("verticallMagnification")
                tooltip: qsTr("verticalMagnification")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.verticalMagnification
                }
                ExpandingSpacer {
                }
            }
        }
    }
}
