import QtQuick 2.1
import HelperWidgets 2.0
import QtQuick.Layouts 1.0

Column {
    width: parent.width

    Section {
        anchors.left: parent.left
        anchors.right: parent.right
        caption: qsTr("General")        

        SectionLayout {
            Label {
                text: qsTr("distanceBarEnabled")
                tooltip: qsTr("distanceBarEnabled")
            }
            SecondColumnLayout {
                CheckBox {
                    backendValue: backendValues.distanceBarEnabled
                }
                ExpandingSpacer {
                }
            }            

            Label {
                text: qsTr("eyeRelativeSize")
                tooltip: qsTr("eyeRelativeSize")
            }
            SecondColumnLayout {
                LineEdit {
                    backendValue: backendValues.eyeRelativeSize
                }
                ExpandingSpacer {
                }
            }
        }
    }
    Section {
        anchors.left: parent.left
        anchors.right: parent.right
        caption: qsTr("backgroundColor")

        ColorEditor {
            caption: qsTr("backgroundColor")
            backendValue: backendValues.backgroundColor
            supportGradient: false
        }
    }

}
