import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgQuick 1.0

T.Button {
    EyeArea {
        id: eyeArea
        anchors.fill: parent
    }
    Connections {
        target: eyeArea
        onAction: {
            checked = true
            clicked()
        }
    }
}




/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
