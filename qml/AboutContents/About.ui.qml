import QtQuick 2.8

Item {
    id: about
    width: 1368
    height: 816

    Rectangle {
        id: background_main
        width: 1368
        height: 816
        color: "#33312d"
    }

    Item {
        id: about_qt
        x: 34
        y: 30
        Image {
            id: about_background
            x: 0
            y: 0
            source: "../../assets/AboutContents/about_background_344_28.png"
        }

        Text {
            id: title_Qt_IA_demo
            x: 30
            y: 13
            color: "#5088ab"
            text: qsTr("Qt Industrial Automation Demo")
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Text {
            id: title_Main_features
            x: 32
            y: 104
            width: 570
            color: "#929495"
            text: qsTr("Main Features")
            wrapMode: Text.WordWrap
            font.family: "Titillium Web"
            font.pixelSize: 30
        }

        Text {
            id: content_Main_features
            x: 32
            y: 168
            width: 570
            height: 90
            color: "#f6f6f6"
            text: qsTr("Design Studio, Qt OPC UA, Qt for Webassembly, QtMQTT.")
            lineHeight: 0.86
            wrapMode: Text.WordWrap
            font.family: "Titillium Web"
            font.pixelSize: 22
        }

        Text {
            id: title_Scenario
            x: 32
            y: 266
            width: 570
            color: "#929495"
            text: qsTr("Scenario")
            wrapMode: Text.WordWrap
            font.family: "Titillium Web"
            font.pixelSize: 30
        }

        Text {
            id: content_Scenario
            x: 32
            y: 330
            width: 570
            height: 400
            color: "#f6f6f6"
            text: qsTr("Qt allows you to create SDK tools for touch panel HMIs, allowing external users (or customers) to define own UIs however you want. The touch panel is used as a client in order to control and retrieve data via Qt OPC UA protocol from a industrial PLC.

With Qt you can also create software application running on a headless device. The UI in the headless device can be accessible via browser, thanks to Qt for Webassembly technology, or via a Qt native application on a tablet. The headless devices can send data via QtMQTT protocol to a server in order to visualize stored data.")
            lineHeight: 0.86
            wrapMode: Text.WordWrap
            font.family: "Titillium Web"
            font.pixelSize: 22
            clip: false
        }
    }

    Item {
        id: what_is_qt
        x: 758
        y: 146
        Image {
            id: what_is_background
            x: 0
            y: 0
            source: "../../assets/AboutContents/what_is_background_344_896.png"
        }

        Text {
            id: title_What_is_Qt
            x: 27
            y: 15
            color: "#5088ab"
            text: qsTr("What is Qt")
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Image {
            id: icon_tools
            x: 68
            y: 110
            source: "../../assets/AboutContents/icon_tools_344_927.png"

            Text {
                id: title_tools
                x: 0
                y: 109
                width: 96
                height: 70
                color: "#929495"
                text: qsTr("Tools
& IDE")
                lineHeight: 0.8
                horizontalAlignment: Text.AlignHCenter
                font.family: "Titillium Web"
                font.pixelSize: 24
            }
        }

        Image {
            id: icon_libraries
            x: 240
            y: 110
            source: "../../assets/AboutContents/icon_libraries_344_929.png"

            Text {
                id: title_libraries
                x: 0
                y: 109
                width: 96
                height: 74
                color: "#929495"
                text: qsTr("Libraries
& APIs")
                lineHeight: 0.8
                horizontalAlignment: Text.AlignHCenter
                font.family: "Titillium Web"
                font.pixelSize: 24
            }
        }

        Image {
            id: icon_community
            x: 412
            y: 120
            source: "../../assets/AboutContents/icon_community_344_931.png"

            Text {
                id: title_community
                x: -15
                y: 99
                width: 126
                height: 74
                color: "#929495"
                text: qsTr("1M+
Community")
                lineHeight: 0.8
                horizontalAlignment: Text.AlignHCenter
                font.family: "Titillium Web"
                font.pixelSize: 24
            }
        }

        Text {
            id: contents_What_is_Qt
            x: 27
            y: 331
            width: 522
            height: 160
            color: "#F6F6f6"
            text: qsTr("A complete cross-platform software framework with ready-made UI elements, C++ libraries, and a complete integrated development environment with tools for everything you need to develop software for any project.")
            lineHeight: 0.86
            wrapMode: Text.WordWrap
            font.family: "Titillium Web"
            font.pixelSize: 22
        }

        Text {
            id: title_The_future
            x: 27
            y: 497
            color: "#CECFD5"
            text: qsTr("The Future is Written with Qt
www.qt.io")
            font.family: "Titillium Web"
            font.pixelSize: 30
        }
    }
}
