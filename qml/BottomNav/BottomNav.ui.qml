import QtQuick 2.10
import QtQuick.Layouts 1.3
import QtQuick.Studio.Components 1.0
import QtQuick.Controls 2.3
import dgautomation 1.0
import ".."

Rectangle {
    id: rectangle
    property alias btn_Status: btn_Status
    property alias btn_Logging: btn_Logging
    property alias btn_Dashboard: btn_Dashboard
    property alias btn_About: btn_About
    property alias btn_Maintenance: btn_Maintenance

    width: 1368
    height: 114
    color: "#00000000"

    TitilliumRegular {
    }
    TitilliumSemiBold {
    }

    Rectangle {
        id: background_main_BottomNav
        width: 1368
        height: 114
        color: "#33312d"
    }

    Row {
        id: navigationRow
        x: 28
        width: 1313
        height: 93
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        spacing: -18

        Btn_Status {
            id: btn_Status
            autoExclusive: true
            checked: true
            checkable: true
        }

        Btn_Logging {
            id: btn_Logging
            autoExclusive: true
            checkable: true
        }

        Btn_Dashboard {
            id: btn_Dashboard
            autoExclusive: true
            checkable: true
        }

        Btn_Maintenance {
            id: btn_Maintenance
            autoExclusive: true
            checkable: true
        }

        Btn_About {
            id: btn_About
            autoExclusive: true
            checkable: true
        }
    }
}
