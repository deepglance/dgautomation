import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_Dashboard
    width: 278
    height: 93

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: dashboardON
        x: 1
        y: 0
        source: "../../assets/BottomNav/lo_btn_selected_195_551_195_551.png"
    }

    Image {
        id: dashboardOFF
        x: 0
        y: 0
        source: "../../assets/BottomNav/lo_btn_195_11_195_11.png"
    }

    Text {
        id: dashboardTXT
        x: 61
        y: 23
        color: "#ffffff"
        text: qsTr("Dashboard")
        lineHeight: 0.9
        font.family: "Titillium Web"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 28
        font.capitalization: Font.AllUppercase
    }

    states: [
        State {
            name: "normal"
            when: !btn_Dashboard.checked && !btn_Dashboard.pressed

            PropertyChanges {
                target: dashboardON
                visible: false
            }
        },

        State {
            name: "down"
            when: btn_Dashboard.pressed

            PropertyChanges {
                target: dashboardOFF
                visible: false
            }

            PropertyChanges {
                target: dashboardTXT
                x: 50
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        },

        State {
            name: "checked"
            when: !btn_Dashboard.pressed && btn_Dashboard.checked

            PropertyChanges {
                target: btn_Dashboard
                down: false
            }

            PropertyChanges {
                target: dashboardOFF
                visible: false
            }

            PropertyChanges {
                target: dashboardTXT
                x: 50
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        }
    ]
}
