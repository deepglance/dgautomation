import QtQuick 2.10
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_About
    width: 276
    height: 93

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: aboutON
        x: 1
        y: 0
        source: "../../assets/BottomNav/ab_btn_selected_195_557_195_557.png"
    }

    Image {
        id: aboutOFF
        x: 0
        y: 0
        source: "../../assets/BottomNav/ab_btn_195_5_195_5.png"
    }

    Text {
        id: aboutTXT
        x: 95
        y: 23
        color: "#ffffff"
        text: qsTr("About")
        horizontalAlignment: Text.AlignHCenter
        font.family: "Titillium Web"
        font.pixelSize: 28
        font.capitalization: Font.AllUppercase
    }

    states: [
        State {
            name: "normal"
            when: !btn_About.checked && !btn_About.pressed

            PropertyChanges {
                target: aboutON
                visible: false
            }
        },
        State {
            name: "down"
            when: btn_About.pressed

            PropertyChanges {
                target: aboutOFF
                visible: false
            }

            PropertyChanges {
                target: aboutTXT
                x: 89
                y: 19
                font.pixelSize: 32
                font.weight: Font.Medium
            }
        },
        State {
            name: "checked"
            when: !btn_About.pressed && btn_About.checked

            PropertyChanges {
                target: btn_About
                down: false
            }

            PropertyChanges {
                target: aboutOFF
                visible: false
            }

            PropertyChanges {
                target: aboutTXT
                x: 89
                y: 19
                color: "#ffffff"
                font.pixelSize: 32
                font.weight: Font.Medium
            }
        }
    ]
}
