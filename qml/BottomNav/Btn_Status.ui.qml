import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_Status
    width: 276
    height: 93

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: statusON
        x: 0
        y: 0
        source: "../../assets/BottomNav/ma_btn_selected_195_17_195_17.png"
    }

    Image {
        id: statusOFF
        x: -1
        y: 0
        source: "../../assets/BottomNav/st_btn_195_13_195_13.png"
    }

    Text {
        id: statusTXT
        x: 77
        y: 23
        width: 123
        height: 40
        color: "#ffffff"
        text: qsTr("Status")
        font.family: "Titillium Web"
        font.wordSpacing: 0
        font.capitalization: Font.AllUppercase
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 28
    }

    states: [
        State {
            name: "normal"
            when: !btn_Status.checked && !btn_Status.pressed

            PropertyChanges {
                target: statusON
                x: 0
                visible: false
            }

            PropertyChanges {
                target: statusOFF
                x: -1
            }
        },
        State {
            name: "down"
            when: btn_Status.pressed

            PropertyChanges {
                target: statusOFF
                visible: false
            }

            PropertyChanges {
                target: statusTXT
                x: 77
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        },
        State {
            name: "checked"
            when: !btn_Status.pressed && btn_Status.checked

            PropertyChanges {
                target: btn_Status
                down: false
            }

            PropertyChanges {
                target: statusOFF
                visible: false
            }

            PropertyChanges {
                target: statusTXT
                x: 77
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        }
    ]
}
