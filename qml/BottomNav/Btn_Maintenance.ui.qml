import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_Maintenance
    width: 278
    height: 93

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: maintenanceON
        x: 1
        y: 0
        source: "../../assets/BottomNav/lo_btn_selected_195_551_195_551.png"
    }

    Image {
        id: maintenanceOFF
        x: 0
        y: 0
        source: "../../assets/BottomNav/lo_btn_195_11_195_11.png"
    }

    Text {
        id: maintenanceTXT
        x: 48
        y: 23
        color: "#ffffff"
        text: qsTr("Maintenance")
        font.family: "Titillium Web"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 28
        font.capitalization: Font.AllUppercase
    }

    states: [
        State {
            name: "normal"
            when: !btn_Maintenance.checked && !btn_Maintenance.pressed

            PropertyChanges {
                target: maintenanceON
                visible: false
            }

            PropertyChanges {
                target: maintenanceTXT
                x: 48
                y: 23
            }
        },
        State {
            name: "down"
            when: btn_Maintenance.pressed

            PropertyChanges {
                target: maintenanceOFF
                visible: false
            }

            PropertyChanges {
                target: maintenanceTXT
                x: 34
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        },
        State {
            name: "checked"
            when: !btn_Maintenance.pressed && btn_Maintenance.checked

            PropertyChanges {
                target: btn_Maintenance
                down: false
            }

            PropertyChanges {
                target: maintenanceOFF
                visible: false
            }

            PropertyChanges {
                target: maintenanceTXT
                x: 34
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        }
    ]
}
