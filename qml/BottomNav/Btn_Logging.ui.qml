import QtQuick 2.10
//import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_Logging
    width: 278
    height: 93

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: loggingON
        x: 1
        y: 0
        source: "../../assets/BottomNav/lo_btn_selected_195_551_195_551.png"
    }

    Image {
        id: loggingOFF
        x: 0
        y: 0
        source: "../../assets/BottomNav/lo_btn_195_11_195_11.png"
    }

    Text {
        id: loggingTXT
        x: 85
        y: 22
        color: "#ffffff"
        text: qsTr("Logging")
        font.family: "Titillium Web"
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        font.capitalization: Font.AllUppercase
        font.pixelSize: 28
    }

    states: [
        State {
            name: "normal"
            when: !btn_Logging.checked && !btn_Logging.pressed

            PropertyChanges {
                target: loggingON
                visible: false
            }
        },
        State {
            name: "down"
            when: btn_Logging.pressed

            PropertyChanges {
                target: loggingOFF
                visible: false
            }

            PropertyChanges {
                target: loggingTXT
                x: 77
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        },
        State {
            name: "checked"
            when: !btn_Logging.pressed && btn_Logging.checked

            PropertyChanges {
                target: btn_Logging
                down: false
            }

            PropertyChanges {
                target: loggingOFF
                visible: false
            }

            PropertyChanges {
                target: loggingTXT
                x: 76
                y: 19
                font.weight: Font.Medium
                font.pixelSize: 32
            }
        }
    ]
}
