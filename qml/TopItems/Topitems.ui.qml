import QtQuick 2.8

Item {
    id: topitems
    width: 1368
    height: 300

    Rectangle {
        id: background_main_TopItems
        width: 1368
        height: 94
        color: "#33312d"
    }

    Item {
        id: top_contents
        x: 0
        y: 0
        width: 1368
        height: 300

        ComboBox {
            x: 1158
            y: 28
            visible: true
            model: ["Monitor", "Operator", "Service"]
        }

        ComboBox {
            x: 962
            y: 28
            visible: true
            model: ["English", "Chinese"]
        }

        Image {
            id: builtwithqt_logo
            x: 770
            y: 38
            source: "assets/builtwithqt_426_23.png"
        }

        Image {
            id: basyskom_logo
            x: 530
            y: 42
            source: "assets/basyskom_426_22.png"
        }

        Running_stop_indicator {
            x: 34
            y: 26
            visible: true
        }
    }

}
