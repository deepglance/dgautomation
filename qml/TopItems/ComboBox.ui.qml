import QtQuick 2.8
import QtQuick.Controls 2.3

ComboBox {
    id: comboBoxLanguage
    width: 190
    height: 56

    font.family: "Titillium Web"
    font.pixelSize: 24
    font.capitalization: Font.AllUppercase

    background: Rectangle {
        anchors.fill: parent
        color: "#26282A"
        border.color: "#1B1C1D"
        border.width: 4
    }
    indicator: Item {}
    contentItem: Text {
        leftPadding: 0
        rightPadding: 0
        text: comboBoxLanguage.displayText
        font: comboBoxLanguage.font
        color: "white"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
    popup.y: comboBoxLanguage.height - 4
    popup.contentItem: ListView {
        clip: true
        implicitHeight: contentHeight
        model: comboBoxLanguage.delegateModel
        currentIndex: comboBoxLanguage.highlightedIndex
        highlightMoveDuration: 0
    }
    popup.background: Rectangle {
        anchors.fill: parent
        color: "#26282A"
        border.color: "#1B1C1D"
        border.width: 4
    }
    delegate: ItemDelegate {
        width: comboBoxLanguage.width
        contentItem: Text {
            text: modelData
            color: comboBoxLanguage.currentIndex === index ? "#21be2b" : "white"
            font: comboBoxLanguage.font
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        background: Item {}
        highlighted: comboBoxLanguage.highlightedIndex === index
    }
}
