import QtQuick 2.8

Item {
    id: running_stop_indicator
    width: 400
    height: 58

    state: "Run Automatic"

    Text {
        id: running_manual_TXT
        x: 84
        y: 7
        color: "#929495"
        text: "RUNNING (Manual)"
        font.pixelSize: 30
        font.family: "Titillium Web"
    }

    Text {
        id: running_automatic_TXT
        x: 84
        y: 7
        color: "#929495"
        text: "RUNNING (Automatic)"
        font.pixelSize: 30
        font.family: "Titillium Web"
    }

    Image {
        id: running_icon
        x: 0
        y: 5
        source: "assets/running_icon_426_25.png"
    }

    Text {
        id: stopped_TXT
        x: 84
        y: 7
        color: "#929495"
        text: "Stopped"
        font.capitalization: Font.AllUppercase
        font.pixelSize: 30
        font.family: "Titillium Web"
    }

    Image {
        id: stopped_icon
        x: 4
        y: 1
        source: "assets/stopped_icon_426_782.png"
    }
    states: [
        State {
            name: "Run Manual"

            PropertyChanges {
                target: running_automatic_TXT
                visible: false
            }

            PropertyChanges {
                target: stopped_TXT
                visible: false
            }

            PropertyChanges {
                target: stopped_icon
                visible: false
            }
        },
        State {
            name: "Run Automatic"

            PropertyChanges {
                target: running_manual_TXT
                visible: false
            }

            PropertyChanges {
                target: stopped_TXT
                visible: false
            }

            PropertyChanges {
                target: stopped_icon
                visible: false
            }
        },
        State {
            name: "Stopped"

            PropertyChanges {
                target: running_icon
                visible: false
            }

            PropertyChanges {
                target: running_automatic_TXT
                visible: false
            }

            PropertyChanges {
                target: running_manual_TXT
                visible: false
            }
        }
    ]

}
