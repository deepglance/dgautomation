import QtQuick 2.10
import DgAutomation 1.0

Item {
    id: listContainer
    width: 858
    height: 42

    Rectangle {
        id: rowBackground
        width: 858
        height: 40
        color: "#46484a"
    }

    Row {
        id: row
        x: 28
        y: 2
        width: 828
        height: 40
        layoutDirection: Qt.LeftToRight
        spacing: 40

        Text {
            id: info
            y: -5
            width: 610
            color: "#f6f6f6"
            text: statisticsInfo
            horizontalAlignment: Text.AlignLeft
            font.weight: Font.Normal
            font.family: "Titillium Web"
            font.pixelSize: 28
        }

        Text {
            id: value
            x: 750
            y: -7
            height: 40
            color: "#f6f6f6"
            text: statisticsValue
            horizontalAlignment: Text.AlignLeft
            font.weight: Font.Normal
            font.family: "Titillium Web"
            font.pixelSize: 32
        }
    }
    states: [
        State {
            name: "rowA"
            when: (index % 2) === 0
        },
        State {
            name: "rowB"
            when: (index % 2) === 1

            PropertyChanges {
                target: rowBackground
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: info
            }
        }
    ]
}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
