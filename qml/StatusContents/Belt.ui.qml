import QtQuick 2.10
import DgAutomation 1.0
import QtQuick.Layouts 1.3
import QtQuick.Timeline 1.0

Item {
    width: 190
    height: 134

    Rectangle {
        id: clippingMask
        x: 24
        y: 14
        width: 150
        height: 60
        color: "#00000000"
        clip: true

        Item {
            id: belt
            x: -10
            width: 170
            height: 60

            Rectangle {
                id: rectangle8
                x: 160
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle7
                x: 140
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle6
                x: 120
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle5
                x: 100
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle4
                x: 80
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle3
                x: 60
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle2
                x: 40
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle1
                x: 20
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }

            Rectangle {
                id: rectangle
                x: 0
                y: 0
                width: 10
                height: 60
                color: "#bcbdbe"
            }
        }
    }

    Timeline {
        id: belt_Timeline
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                property: "currentFrame"
                running: true
                to: 500
                duration: 500
                loops: -1
                from: 0
            }
        ]
        endFrame: 500
        startFrame: 0
        enabled: true

        KeyframeGroup {
            target: belt
            property: "x"

            Keyframe {
                frame: 500
                value: 10
            }
        }
    }

    Btn_ToggleH {
        id: btn_ToggleH
        x: 10
        y: 0
        visible: false
    }
    states: [
        State {
            name: "toggleH ON"
            when: btn_Manual.checked

            PropertyChanges {
                target: btn_ToggleH
                visible: true
            }
        }
    ]
}




/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
