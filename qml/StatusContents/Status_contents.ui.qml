import QtQuick 2.8
import QtQuick.Layouts 1.3
import DgQuick 1.0

Item {
    id: status_contents
    width: 1368
    height: 816

    Rectangle {
        id: background_main
        width: 1368
        height: 816
        color: "#33312d"
    }

    Item {
        id: machine
        x: 29
        y: 24

        Image {
            id: bg_Machine
            x: 4
            y: 6
            visible: true
            source: "../../assets/StatusContents/machine_background_396_28.png"
        }

        Text {
            id: title_Machine
            x: 38
            y: 18
            color: "#5088ab"
            text: "Machine"
            fontSizeMode: Text.Fit
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Cogs {
            id: cogs1
            x: 43
            y: 106
        }

        Belt {
            id: belt1
            x: 184
            y: 146
        }

        Cogs {
            id: cogs2
            x: 383
            y: 106
        }

        Belt {
            id: belt2
            x: 525
            y: 146
        }

        Cogs {
            id: cogs3
            x: 725
            y: 106
        }

        Text {
            id: _0__396_485
            x: 412
            y: 292
            color: "#D91C1C"
            text: "88.0%"
            font.weight: Font.Medium
            visible: false
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Text {
            id: _5__396_490
            x: 74
            y: 292
            color: "#21BE2B"
            text: "91.5%"
            font.pixelSize: 40
            font.weight: Font.Medium
            visible: false
            font.family: "Titillium Web"
        }
    }

    Item {
        id: video
        x: 956
        y: 24
        Image {
            id: bg_Video
            x: 5
            y: 6
            source: "../../assets/StatusContents/video_396_87.png"
        }

        Text {
            id: title_Video
            x: 38
            y: 18
            color: "#5088ab"
            text: "Video"
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Item {
            id: videos
            x: 18
            y: 82
            width: 348
            height: 250

            AnimatedImage {
                id: production1_halfed
                x: 0
                y: 0
                visible: true
                source: "../../assets/StatusContents/production1_halfed_348x250.gif"
            }

            AnimatedImage {
                id: production2_halfed
                x: 0
                y: 0
                visible: true
                source: "../../assets/StatusContents/production2_halfed_348x250.gif"
            }

            AnimatedImage {
                id: production3_halfed
                x: 0
                y: 0
                visible: true
                source: "../../assets/StatusContents/production3_halfed_348x250.gif"
            }

            AnimatedImage {
                id: transport1_halfed
                x: 0
                y: 0
                visible: true
                source: "../../assets/StatusContents/transport1_halfed_348x250.gif"
            }

            AnimatedImage {
                id: transport2_halfed
                x: 0
                y: 0
                visible: true
                source: "../../assets/StatusContents/transport2_halfed_348x250.gif"
            }

            Image {
                id: shade
                x: 0
                y: 0
                source: "../../assets/StatusContents/video_shade.png"
            }
        }
    }

    Item {
        id: statistics
        x: 54
        y: 400

        Text {
            id: title_Statistics
            x: 12
            y: 14
            color: "#5088ab"
            text: "Statistics"
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Rectangle {
            id: listClippingMask
            y: 88
            width: 858
            height: 298
            color: "#00000000"
            clip: true

            ListView {
                id: listView
                x: 0
                y: 0
                width: 858
                height: 292

                model: ListModel {
                    ListElement {
                        statisticsInfo: "Throughput per hour"
                        statisticsValue: "200"
                    }

                    ListElement {
                        statisticsInfo: "Parts Produced (last hour)"
                        statisticsValue: "8"
                    }

                    ListElement {
                        statisticsInfo: "Availability (last hour)"
                        statisticsValue: "120.4%"
                    }
                    ListElement {
                        statisticsInfo: "Throughput per hour"
                        statisticsValue: "200"
                    }

                    ListElement {
                        statisticsInfo: "Parts Produced (last hour)"
                        statisticsValue: "8"
                    }

                    ListElement {
                        statisticsInfo: "Availability (last hour)"
                        statisticsValue: "120.4%"
                    }
                    ListElement {
                        statisticsInfo: "Throughput per hour"
                        statisticsValue: "200"
                    }

                    ListElement {
                        statisticsInfo: "Parts Produced (last hour)"
                        statisticsValue: "8"
                    }

                    ListElement {
                        statisticsInfo: "Availability (last hour)"
                        statisticsValue: "120.4%"
                    }

                    ListElement {
                        statisticsInfo: "Throughput per hour"
                        statisticsValue: "200"
                    }

                    ListElement {
                        statisticsInfo: "Parts Produced (last hour)"
                        statisticsValue: "8"
                    }

                    ListElement {
                        statisticsInfo: "Availability (last hour)"
                        statisticsValue: "120.4%"
                    }
                }

                delegate: StatisticsListDelegate {
                    id: listDelegate
                }
            }
            EyeArea {
                id: scroll
                x: listView.x
                y: listView.y
                width: listView.width
                height: listView.height
                scrollEnabled: true
                scrollDeadSubArea: "0.3,0.3,0.4x0.4"
                verticalMagnification: 1.2
            }
        }
    }

    Item {
        id: side_buttons
        x: 955
        y: 394

        Btn_Stop {
            id: btn_Stop
            x: 0
            y: 1
            autoExclusive: true
            checkable: true
        }

        Btn_Automatic {
            id: btn_Automatic
            x: 0
            y: 151
            autoExclusive: true
            checkable: true
        }

        Btn_Manual {
            id: btn_Manual
            x: 0
            y: 267
            checkable: true
            autoExclusive: true
        }
    }

    Connections {
        target: scroll
        onScroll: {
            listView.contentX = Math.max(0, Math.min(listView.contentWidth - listView.width, listView.contentX + (dx * 3000)))
            listView.contentY = Math.max(0, Math.min(listView.contentHeight - listView.height, listView.contentY - (dy * 3000)))
        }
    }
}
