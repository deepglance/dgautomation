import QtQuick 2.10
import DgAutomation 1.0
import QtQuick.Timeline 1.0

Item {
    id: successAnimation
    width: 140
    height: 48

    Text {
           id: txtSuccess
           x: 0
           y: 0
           width: 140
           height: 61
           color: "#21be2b"
           text: qsTranslate("", "98.2%")
           horizontalAlignment: Text.AlignHCenter
           font.weight: Font.Medium
           font.family: "Titillium Web"
           font.pixelSize: 40
    }

    Timeline {
        id: sucessTimeline
        animations: [
               TimelineAnimation {
                   id: timelineAnimation
                   property: "currentFrame"
                   from: 0
                   to: 3000
                   running: true
                   duration: 3000
                   loops: 1
               }
           ]
           startFrame: 0
           endFrame: 3000
           enabled: true


           KeyframeGroup {
               target: txtSuccess
               property: "opacity"

               Keyframe {
                   value: 0
                   frame: 0
               }

               Keyframe {
                   value: 1
                   frame: 500
               }

               Keyframe {
                   value: 1
                   frame: 2500
               }

               Keyframe {
                   value: 0
                   frame: 3000
               }


           }
       }

}

/*##^## Designer {
    D{i:2;currentFrame__AT__NodeInstance:3}
}
 ##^##*/
