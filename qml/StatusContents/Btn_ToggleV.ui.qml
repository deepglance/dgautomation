import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_ToggleV
    width: 140
    height: 220

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Text {
        id: toggleV_TXT
        x: 32
        y: 172
        color: "#51b6f4"
        text: qsTr("Toggle")
        horizontalAlignment: Text.AlignHCenter
        font.family: "Titillium Web"
        font.pixelSize: 28
    }

    Rectangle {
        id: rectangle
        width: 140
        height: 220
        color: "#00000000"
        border.width: 2
        border.color: "#43adee"
    }

    states: [
        State {
            name: "normal"
            when: !btn_ToggleV.down
        },
        State {
            name: "down"
            when: btn_ToggleV.down

            PropertyChanges {
                target: rectangle
                color: "#26000000"
                border.color: "#3c82ae"
            }

            PropertyChanges {
                target: toggleV_TXT
                color: "#459bd0"
            }
        }
    ]
}
