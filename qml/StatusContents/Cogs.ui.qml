import QtQuick 2.8
import QtQuick.Timeline 1.0

Item {
    id: cogs
    width: 140
    height: 220

    Item {
        id: cogSmall
        x: 14
        y: 99
        width: 68
        height: 66

        Image {
            id: cogsmall_860_11
            x: 2
            y: 2
            source: "../../assets/StatusContents/cogsmall_860_11.png"
        }
    }

    Item {
        id: cogBig
        x: 28
        y: 14
        width: 100
        height: 100

        Image {
            id: cogbig_860_12
            x: 2
            y: 2
            source: "../../assets/StatusContents/cogbig_860_12.png"
        }
    }

    TxtSuccess {
        id: animationSuccess
        x: 0
        y: 172
        visible: false
    }

    TxtFailure {
        id: animationFailure
        y: 172
        visible: false
    }

    Timeline {
        id: cogAnimation
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                property: "currentFrame"
                from: 0
                duration: 500
                running: true
                to: 500
                loops: -1
            }
        ]
        enabled: true
        endFrame: 500
        startFrame: 0

        KeyframeGroup {
            target: cogBig
            property: "rotation"

            Keyframe {
                frame: 500
                value: -45
            }
        }

        KeyframeGroup {
            target: cogSmall
            property: "rotation"

            Keyframe {
                frame: 500
                value: 60
            }
        }
    }

    Btn_ToggleV {
        id: btn_ToggleV
        visible: false
    }

    states: [
        State {
            name: "toggleV ON"
            when: btn_Manual.checked

            PropertyChanges {
                target: btn_ToggleV
                visible: true
            }
        },
        State {
            name: "Success"

            PropertyChanges {
                target: animationSuccess
                visible: true
            }
        },
        State {
            name: "Failure"

            PropertyChanges {
                target: animationFailure
                visible: true
            }
        }
    ]
}




/*##^## Designer {
    D{i:7}
}
 ##^##*/
