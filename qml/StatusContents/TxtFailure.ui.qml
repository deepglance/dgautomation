import QtQuick 2.10
import DgAutomation 1.0
import QtQuick.Timeline 1.0

Item {
    id: failureAnimation
    width: 140
    height: 48

    Text {
        id: txtFailure
        x: 0
        y: 0
        width: 140
        height: 61
        color: "#d91c1c"
        text: "51.4%"
        horizontalAlignment: Text.AlignHCenter
        font.weight: Font.Medium
        font.family: "Titillium Web"
        font.pixelSize: 40
    }

    Timeline {
        id: failureTimeline
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                property: "currentFrame"
                from: 0
                to: 3000
                running: true
                duration: 3000
                loops: 1
            }
        ]
        startFrame: 0
        endFrame: 3000
        enabled: true

        KeyframeGroup {
            target: txtFailure
            property: "opacity"

            Keyframe {
                value: 0
                frame: 0
            }

            Keyframe {
                value: 1
                frame: 500
            }

            Keyframe {
                value: 1
                frame: 2500
            }

            Keyframe {
                value: 0
                frame: 3000
            }
        }
    }
}




/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
