import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_Manual
    width: 386
    height: 134

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: manualON
        x: 1
        y: 1
        source: "../../assets/StatusContents/manual_btn_on_396_409.png"
    }

    Image {
        id: manualOFF
        x: 0
        y: 0
        source: "../../assets/StatusContents/manual_btn_off_396_337.png"
    }

    Text {
        id: manualTXT
        x: 124
        y: 40
        color: "#ffffff"
        text: qsTr("Manual")
        font.family: "Titillium Web"
        font.capitalization: Font.AllUppercase
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 36
    }

    states: [
        State {
            name: "normal"
            when: !btn_Manual.checked && !btn_Manual.pressed

            PropertyChanges {
                target: manualON
                visible: false
            }
        },
        State {
            name: "down"
            when: btn_Manual.pressed

            PropertyChanges {
                target: manualOFF
                visible: false
            }

            PropertyChanges {
                target: manualTXT
                x: 120
                y: 38
                font.weight: Font.Medium
                font.pixelSize: 38
            }
        },
        State {
            name: "checked"
            when: !btn_Manual.pressed && btn_Manual.checked

            PropertyChanges {
                target: btn_Manual
                down: false
            }

            PropertyChanges {
                target: manualOFF
                visible: false
            }

            PropertyChanges {
                target: manualTXT
                x: 120
                y: 38
                font.weight: Font.Medium
                font.pixelSize: 38
            }
        }
    ]
}
