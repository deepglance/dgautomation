import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_Stop
    width: 386
    height: 134

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: stopON
        x: 0
        y: 0
        source: "../../assets/StatusContents/stop_btn_on_396_691.png"
    }

    Image {
        id: stopOFF
        x: 0
        y: 0
        source: "../../assets/StatusContents/stop_btn_off_396_271.png"
    }

    Text {
        id: stopTXT
        x: 152
        y: 40
        color: "#ffffff"
        text: qsTr("STOP")
        font.family: "Titillium Web"
        font.pixelSize: 36
    }

    states: [
        State {
            name: "normal"
            when: !btn_Stop.checked && !btn_Stop.pressed

            PropertyChanges {
                target: stopON
                visible: false
            }

            PropertyChanges {
                target: stopTXT
            }
        },
        State {
            name: "down"
            when: btn_Stop.pressed

            PropertyChanges {
                target: stopTXT
                x: 149
                y: 38
                font.weight: Font.Medium
                font.pixelSize: 38
            }

            PropertyChanges {
                target: stopOFF
                visible: false
            }
        },
        State {
            name: "checked"
            when: !btn_Stop.pressed && btn_Stop.checked

            PropertyChanges {
                target: btn_Stop
                down: false
            }

            PropertyChanges {
                target: stopTXT
                x: 149
                y: 38
                font.weight: Font.Medium
                font.pixelSize: 38
            }

            PropertyChanges {
                target: stopOFF
                visible: false
            }
        }
    ]
}
