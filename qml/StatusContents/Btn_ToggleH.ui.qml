import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"

EyeButton {
    id: btn_ToggleH
    width: 178
    height: 134

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Text {
        id: toggleH_TXT
        x: 51
        y: 86
        color: "#51b6f4"
        text: qsTr("Toggle")
        font.family: "Titillium Web"
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 28
    }

    Rectangle {
        id: rectangle
        width: 178
        height: 134
        color: "#00000000"
        border.color: "#43adee"
        border.width: 2
    }

    states: [
        State {
            name: "normal"
            when: !btn_ToggleH.down
        },
        State {
            name: "down"
            when: btn_ToggleH.down

            PropertyChanges {
                target: rectangle
                color: "#26000000"
                border.color: "#3c82ae"
            }

            PropertyChanges {
                target: toggleH_TXT
                color: "#459bd0"
            }
        }
    ]
}
