import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0
import "../Components"
import DgQuick 1.0

EyeButton {
    id: btn_Automatic
    width: 386
    height: 134

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"

    Image {
        id: automaticON
        x: 1
        y: 1
        source: "../../assets/StatusContents/automatic_btn_on_396_335.png"
    }

    Image {
        id: automaticOFF
        x: 0
        y: 0
        width: 386
        source: "../../assets/StatusContents/automatic_btn_off_396_342.png"
    }

    Text {
        id: automaticTXT
        x: 101
        y: 40
        color: "#ffffff"
        text: qsTr("Automatic")
        font.family: "Titillium Web"
        font.capitalization: Font.AllUppercase
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 36
    }

    states: [
        State {
            name: "normal"
            when: !btn_Automatic.checked && !btn_Automatic.pressed

            PropertyChanges {
                target: automaticON
                visible: false
            }

            PropertyChanges {
                target: automaticTXT
            }
        },
        State {
            name: "down"
            when: btn_Automatic.pressed

            PropertyChanges {
                target: automaticOFF
                visible: false
            }

            PropertyChanges {
                target: automaticTXT
                x: 96
                y: 38
                font.weight: Font.Medium
                font.pixelSize: 38
            }
        },
        State {
            name: "checked"
            when: !btn_Automatic.pressed && btn_Automatic.checked

            PropertyChanges {
                target: btn_Automatic
                down: false
            }

            PropertyChanges {
                target: automaticTXT
                x: 96
                y: 38
                font.weight: Font.Medium
                font.pixelSize: 38
            }

            PropertyChanges {
                target: automaticOFF
                visible: false
            }
        }
    ]
}
