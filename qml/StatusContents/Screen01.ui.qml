import QtQuick 2.10
import DgAutomation 1.0
import ".."

Rectangle {
    width: 1368
    height: 816

    color: Constants.backgroundColor

    TitilliumRegular{
    }
    TitilliumSemiBold{
    }
    TitilliumBold{
    }

    Status_contents {
        id: status_contents
    }
}
