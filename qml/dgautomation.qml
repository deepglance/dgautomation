import QtQuick 2.10
import QtQuick.Controls 2.2
import DgQuick 1.0
import dgautomation 1.0

Item {
    width: Constants.width
    height: Constants.height
    visible: true

    ApplicationFlow {
    }

    EyeTracker {
        model: EyeTracker.ModelTobii
        Component.onCompleted: start()
    }

    EventHandler {
        objectName: "eventHandler"
        id: eventHandler
        anchors.fill: parent
        focus: true
        Keys.onPressed: {
            if (event.key === Qt.Key_T) {
                trackstatus.visible = !trackstatus.visible;
                event.accepted = true;
            }
        }
    }

    ErrorHandler {
        objectName: "errorHandler"
        id: errorHandler
        anchors.fill: parent
        focus: true
        onError: {
            if (error === ErrorHandler.ErrorEyeTrackerSoftwareNotInstalled) {
                errorDialog.message = "Tobii software not installed"
                errorDialog.visible = true
            } else if (error === ErrorHandler.ErrorEyeTrackerNotConnected) {
                errorDialog.message = "Tobii eye tracker not connected"
                errorDialog.visible = true
            }
        }
    }

    TrackStatus {
        id: trackstatus
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.horizontalCenter: parent.horizontalCenter
        width: 100
        height: 60
        visible: false
    }

    Dialog {
        id: errorDialog
        property alias message: message.text
        title: "Error"
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2
        width: 300
        height: 150
        standardButtons: DialogButtonBox.Ok
        Text {
            id: message
        }
    }


}
