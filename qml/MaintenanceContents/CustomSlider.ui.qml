import QtQuick 2.10
import DgAutomation 1.0
import QtQuick.Timeline 1.0
import QtQuick.Controls 2.3

Item {
    id: customSlider
    width: 98
    height: 549
    property alias sliderPressed: slider.pressed

    Image {
        id: custom_slider_bg
        x: 0
        y: 58
        source: "../../assets/MaintenanceContents/sliderbackground_195_280.png"

        Image {
            id: sliderbar_empty
            x: 45
            y: 48
            height: 381
            source: "../../assets/MaintenanceContents/sliderbarempty_195_46.png"
        }
    }

    Image {
        id: sliderbar_full
        x: 36
        y: 106
        width: 26
        height: 381
        source: "../../assets/MaintenanceContents/sliderbarfull_195_278.png"
    }

    Text {
        id: value_slider
        x: 0
        y: -1
        width: 98
        height: 46
        color: "#21be2b"
        text: slider.value
        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: 30
        font.family: "Titillium Web"
    }

    Btn_Tick {
        id: btn_Tick
        x: 15
        y: 72
        visible: true
        pressOverride: slider.pressed

        Image {
            id: value_bg
            x: -12
            y: -72
            source: "../../assets/MaintenanceContents/value_background_195_50.png"
            visible: slider.pressed

            Text {
                id: value_sliderPressed
                x: 0
                y: 6
                width: 92
                height: 46
                color: "#cecfd5"
                text: Math.round(slider.position * 1000)
                horizontalAlignment: Text.AlignHCenter
                font.family: "Titillium Web"
                font.pixelSize: 30
            }
        }
    }

    Slider {
        id: slider
        x: 29
        y: 86
        width: 40
        height: 431
        live: false
        visible: true
        orientation: Qt.Vertical
        opacity: 0
        focusPolicy: Qt.ClickFocus
        stepSize: 1
        value: 0
        to: 1000
    }

    Timeline {
        id: sliderBar_timeline
        currentFrame: slider.position * 1000
        animations: [
            TimelineAnimation {
                id: timelineAnimation
                running: false
                from: 0
                to: 1000
                duration: 1000
                loops: 1
            }
        ]
        startFrame: 0
        endFrame: 1000
        enabled: true

        KeyframeGroup {
            target: sliderbar_full
            property: "height"

            Keyframe {
                frame: 0
                value: 1
            }

            Keyframe {
                frame: 1000
                value: 381
            }
        }

        KeyframeGroup {
            target: sliderbar_full
            property: "y"

            Keyframe {
                frame: 0
                value: 486
            }

            Keyframe {
                frame: 1000
                value: 106
            }
        }

        KeyframeGroup {
            target: btn_Tick
            property: "y"

            Keyframe {
                frame: 0
                value: 462
            }

            Keyframe {
                frame: 1000
                value: 72
            }
        }
    }
}
