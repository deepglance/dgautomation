import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0

T.Button {
    id: btn_Tick
    width: 68
    height: 76
    property bool pressOverride: false

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    Image {
        id: tick_pressed
        x: 0
        y: 0
        source: "../../assets/MaintenanceContents/tickpressed_195_245.png"
    }

    Image {
        id: tick_normal
        x: 0
        y: 0
        source: "../../assets/MaintenanceContents/ticknormal_195_235.png"
    }

    states: [
        State {
            name: "normal"
            when: !btn_Tick.pressOverride

            PropertyChanges {
                target: tick_pressed
                visible: false
            }
        },
        State {
            name: "down"
            when: btn_Tick.pressOverride

            PropertyChanges {
                target: tick_normal
                visible: false
            }
        }
    ]
}
