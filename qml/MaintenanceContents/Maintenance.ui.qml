import QtQuick 2.8

Item {
    id: maintenance
    width: 1368
    height: 816

    Rectangle {
        id: background_main
        width: 1368
        height: 816
        color: "#33312d"
    }

    Item {
        id: machine_configuration
        x: 33
        y: 30

        Image {
            id: machine_background
            x: 0
            y: 0
            source: "../../assets/MaintenanceContents/machine_background_195_22_195_22.png"
        }

        Text {
            id: title_machine_configuration
            x: 30
            y: 13
            color: "#5088ab"
            text: qsTranslate("Translations", "Machine Configuration")
            font.family: "Titillium Web"
            font.pixelSize: 40
        }
        Item {
            id: expected_output
            x: 725
            y: 114
            Image {
                id: output_background
                x: 0
                y: 0
                source: "../../assets/MaintenanceContents/output_background_195_24.png"
            }

            Text {
                id: values_output
                x: 417
                y: 149
                color: "#F3F3F4"
                text: qsTranslate("Translations", "685.1%
4%
66.67%
18.23%
23.35%
75%")
                font.family: "Titillium Web"
                font.pixelSize: 30
            }

            Text {
                id: description_output
                x: 34
                y: 151
                color: "#F3F3F4"
                text: qsTranslate(
                          "Translations",
                          "Total duration  .....................................
Expected Final Quality  .....................
Expected Quality A  ...........................
Expected Quality B  ...........................
Expected Quality C  ...........................
Expected Acceptance Rate .............")
                lineHeight: 1.23
                font.family: "Titillium Web"
                font.pixelSize: 24
            }

            Text {
                id: title_expected_output
                x: 30
                y: 15
                color: "#5088ab"
                text: qsTranslate("Translations", "Expected Output")
                font.family: "Titillium Web"
                font.pixelSize: 40
            }
        }

        Text {
            id: title_machine4
            x: 532
            y: 104
            width: 108
            height: 67
            color: "#BDBEBF"
            text: qsTranslate("Translations", "Accepted
Quality")
            lineHeight: 0.9
            horizontalAlignment: Text.AlignHCenter
            font.family: "Titillium Web"
            font.pixelSize: 24
        }

        Text {
            id: title_machine3
            x: 368
            y: 104
            width: 108
            height: 67
            color: "#BDBEBF"
            text: qsTranslate("Translations", "Machine 3
Duration")
            lineHeight: 0.9
            horizontalAlignment: Text.AlignHCenter
            font.family: "Titillium Web"
            font.pixelSize: 24
        }

        Text {
            id: title_machine2
            x: 204
            y: 104
            width: 108
            color: "#BDBEBF"
            text: qsTranslate("Translations", "Machine 2
Duration")
            lineHeight: 0.9
            horizontalAlignment: Text.AlignHCenter
            font.family: "Titillium Web"
            font.pixelSize: 24
        }

        Text {
            id: title_machine1
            x: 40
            y: 104
            width: 108
            color: "#BDBEBF"
            text: qsTranslate("Translations", "Machine 1
Duration")
            lineHeight: 0.9
            horizontalAlignment: Text.AlignHCenter
            font.family: "Titillium Web"
            font.pixelSize: 24
        }

        CustomSlider {
            id: customSlider4
            x: 537
            y: 180
        }

        CustomSlider {
            id: customSlider3
            x: 373
            y: 180
        }

        CustomSlider {
            id: customSlider2
            x: 209
            y: 180
        }

        CustomSlider {
            id: customSlider1
            x: 45
            y: 180
        }
    }
}
