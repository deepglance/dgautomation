import QtQuick 2.10
import DgAutomation 1.0

Item {
    id: logging_ListDelegate
    width: 1040
    height: 42

    Rectangle {
        id: timelineRow
        width: 280
        height: 40
        color: "#46484a"
    }

    Rectangle {
        id: levelRow
        x: 282
        y: 0
        width: 160
        height: 40
        color: "#46484a"

        Rectangle {
            id: warningIMG
            x: 140
            y: 0
            width: 20
            height: 40
            color: "#f4be04"
        }

        Rectangle {
            id: errorIMG
            x: 140
            y: 0
            width: 20
            height: 40
            color: "#bd181e"
        }
    }

    Rectangle {
        id: messageRow
        x: 444
        y: 0
        width: 596
        height: 40
        color: "#46484a"
    }

    Row {
        id: row
        x: 26
        y: 0
        width: 990
        height: 40
        spacing: 50
        clip: true

        Text {
            id: timestamp_info
            x: 0
            y: 1
            width: 230
            height: 35
            color: "#f6f6f6"
            text: timestamp
            clip: true
            horizontalAlignment: Text.AlignLeft
            font.family: "Titillium Web"
            font.pixelSize: 24
        }

        Text {
            id: level_info
            x: 0
            y: 1
            width: 110
            height: 35
            color: "#f6f6f6"
            text: level? level: ""
            clip: true
            font.family: "Titillium Web"
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: 24
        }

        Text {
            id: message_info
            x: 0
            y: 1
            width: 546
            height: 35
            color: "#f6f6f6"
            text: message? message: ""
            clip: true
            font.family: "Titillium Web"
            horizontalAlignment: Text.AlignLeft
            font.pixelSize: 24
        }
    }


    states: [
        State {
            name: "rowA"
            when: (index % 4) === 0

            PropertyChanges {
                target: errorIMG
                visible: false
            }

            PropertyChanges {
                target: warningIMG
                visible: false
            }
        },
        State {
            name: "rowB"
            when: (index % 4) === 1

            PropertyChanges {
                target: timelineRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: levelRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: messageRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: warningIMG
                visible: false
            }

            PropertyChanges {
                target: errorIMG
                visible: false
            }
        },
        State {
            name: "warningA"
            when: (index % 4) === 2

            PropertyChanges {
                target: errorIMG
                visible: false
            }
        },
        State {
            name: "warningB"
            PropertyChanges {
                target: timelineRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: levelRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: messageRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: errorIMG
                visible: false
            }
        },
        State {
            name: "errorA"

            PropertyChanges {
                target: warningIMG
                visible: false
            }
        },
        State {
            name: "errorB"
            when: (index % 4) === 3
            PropertyChanges {
                target: timelineRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: levelRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: messageRow
                color: "#3b3c3e"
            }

            PropertyChanges {
                target: warningIMG
                visible: false
            }
        }
    ]
}
