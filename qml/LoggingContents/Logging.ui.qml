import QtQuick 2.8
import DgQuick 1.0

Item {
    id: logging
    width: 1368
    height: 816

    Rectangle {
        id: background_main
        width: 1368
        height: 816
        color: "#33312d"
    }

    Item {
        id: logging_info
        x: 34
        y: 30
        Image {
            id: filters_background
            x: 1074
            y: 20
            source: "../../assets/LoggingContents/filters_background.png"
        }

        Image {
            id: logging_background
            x: 0
            y: 0
            source: "../../assets/LoggingContents/logging_background_195_28.png"
        }

        Text {
            id: title_Message
            x: 472
            y: 11
            color: "#5088ab"
            text: "Message"
            font.pixelSize: 40
            font.family: "Titillium Web"
        }

        Text {
            id: title_Level
            x: 310
            y: 11
            color: "#5088ab"
            text: "Level"
            font.pixelSize: 40
            font.family: "Titillium Web"
        }

        Text {
            id: title_Timestamp
            x: 28
            y: 11
            color: "#5088ab"
            text: "Timestamp"
            font.pixelSize: 40
            font.family: "Titillium Web"
        }

        ListView {
            id: loggingListView
            x: 21
            y: 86
            width: 1040
            height: 662
            clip: true
            delegate: LoggingListDelegate {
                id: listDelegate
            }

            model: ListModel {
                ListElement {
                    timestamp: "2018-08-01 14:00:52"
                    level: "Note"
                    message: "Station B manufacturing duration changed to 20 secs."
                }
                ListElement {
                    timestamp: "2018-08-01 14:02:05"
                    level: "Note"
                    message: "Status changed to Automatic"
                }
                ListElement {
                    timestamp: "2018-08-01 14:12:26"
                    level: "Warning"
                    message: "Emergency Stop activated"
                }
                ListElement {
                    timestamp: "2018-08-01 14:16:24"
                    level: "Error"
                    message: "Station A manufacturing duration changed to 40 secs."
                }
                ListElement {
                    timestamp: "2018-08-01 14:47:27"
                    level: "Note"
                    message: "Status changed to Automatic"
                }
                ListElement {
                    timestamp: "2018-08-01 14:00:52"
                    level: "Note"
                    message: "Emergency Stop activated"
                }
                ListElement {
                    timestamp: "2018-08-01 14:02:05"
                }
                ListElement {
                    timestamp: "2018-08-01 14:12:26"
                }
                ListElement {
                    timestamp: "2018-08-01 14:16:24"
                }
                ListElement {
                    timestamp: "2018-08-01 14:47:27"
                }
                ListElement {
                    timestamp: "2018-08-01 14:00:52"
                }
                ListElement {
                    timestamp: "2018-08-01 14:02:05"
                }
                ListElement {
                    timestamp: "2018-08-01 14:12:26"
                }
                ListElement {
                    timestamp: "2018-08-01 14:16:24"
                }
                ListElement {
                    timestamp: "2018-08-01 14:47:27"
                }
                ListElement {
                    timestamp: "2018-08-01 14:00:52"
                }
                ListElement {
                    timestamp: "2018-08-01 14:02:05"
                }
                ListElement {
                    timestamp: "2018-08-01 14:12:26"
                }
                ListElement {
                    timestamp: "2018-08-01 14:16:24"
                }
                ListElement {
                    timestamp: "2018-08-01 14:47:27"
                }
                ListElement {
                    timestamp: "2018-08-01 14:00:52"
                }
                ListElement {
                    timestamp: "2018-08-01 14:02:05"
                }
                ListElement {
                    timestamp: "2018-08-01 14:12:26"
                }
                ListElement {
                    timestamp: "2018-08-01 14:16:24"
                }
                ListElement {
                    timestamp: "2018-08-01 14:47:27"
                }
            }
        }
        EyeArea {
            id: scroll
            x: loggingListView.x
            y: loggingListView.y
            width: loggingListView.width
            height: loggingListView.height
            scrollEnabled: true
            scrollDeadSubArea: "0.3,0.3,0.4x0.4"
            verticalMagnification: 1.1
        }
    }

    Item {
        id: filter_source
        x: 1144
        y: 106

        Text {
            id: filter_sourceTXT
            x: 0
            y: 8
            color: "#5088ab"
            text: "Filter Source"
            font.pixelSize: 28
            font.family: "Titillium Web"
        }

        Btn_Filter {
            id: btn_FilterMachine
            text: "Machine"
            x: 0
            y: 86
        }

        Btn_Filter {
            id: btn_FilterUI
            y: 158
            text: "UI"
        }



        Btn_Filter {
            id: btn_FilterBackend
            y: 230
            text:"Backend"
        }

    }

    Item {
        id: filter_level
        x: 1144
        y: 442
        width: 200
        height: 200

        Text {
            id: filter_levelTXT
            x: 0
            y: 8
            color: "#5088ab"
            text: "Filter Level"
            font.pixelSize: 28
            font.family: "Titillium Web"
        }

        Btn_Filter {
            id: btn_FilterNote
            y: 86
            text: "Note"
        }

        Btn_Filter {
            id: btn_FilterWarning
            y: 158
            text: "Warning"
        }

        Btn_Filter {
            id: btn_FilterError
            y: 230
            text: "Error"
        }

    }

    Connections {
        target: scroll
        onScroll: {
            loggingListView.contentX = Math.max(0, Math.min(loggingListView.contentWidth - loggingListView.width, loggingListView.contentX + (dx * 3000)))
            loggingListView.contentY = Math.max(0, Math.min(loggingListView.contentHeight - loggingListView.height, loggingListView.contentY - (dy * 3000)))
        }
    }
}
