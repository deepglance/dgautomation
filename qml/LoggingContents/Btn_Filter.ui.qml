import QtQuick 2.10
import QtQuick.Templates 2.1 as T
import DgAutomation 1.0

T.Button {
    id: btn_Filter
    width: 224
    height: 74

    font: Constants.font
    implicitWidth: Math.max(
                       background ? background.implicitWidth : 0,
                       contentItem ? contentItem.implicitWidth + leftPadding
                                     + rightPadding : leftPadding + rightPadding)
    implicitHeight: Math.max(
                        background ? background.implicitHeight : 0,
                        contentItem ? contentItem.implicitHeight + topPadding
                                      + bottomPadding : topPadding + bottomPadding)
    leftPadding: 4
    rightPadding: 4

    text: "My Button"
    hoverEnabled: false
    checkable: true

    Image {
        id: filterOFF
        x: 126
        y: 0
        source: "../../assets/LoggingContents/btn_filterOFF.png"
    }

    Image {
        id: filterON
        x: 127
        y: 1
        source: "../../assets/LoggingContents/btn_filterON.png"
    }

    Text {
        id: filterTXT
        x: 2
        y: 18
        color: "#f6f6f6"
        text: parent.text
        font.capitalization: Font.AllUppercase
        font.family: "Titillium Web"
        font.pixelSize: 24
    }



    states: [
        State {
            name: "normal"
            when: !btn_Filter.checked && !btn_Filter.pressed

            PropertyChanges {
                target: filterON
                visible: false
            }
        },
        State {
            name: "down"
            when: btn_Filter.pressed

            PropertyChanges {
                target: filterTXT
                color: "#21be2b"
            }

            PropertyChanges {
                target: filterOFF
                visible: false
            }
        },
        State {
            name: "checked"
            when: !btn_Filter.pressed && btn_Filter.checked

            PropertyChanges {
                target: filterTXT
                color: "#21be2b"
            }

            PropertyChanges {
                target: filterOFF
                visible: false
            }
        }
    ]
}
