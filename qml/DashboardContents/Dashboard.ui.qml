import QtQuick 2.8

Item {
    id: dashboard_195_585
    width: 1368
    height: 816

    Rectangle {
        id: background_main
        width: 1368
        height: 816
        color: "#33312d"
    }

    Item {
        id: performance
        x: 33
        y: 30
        Image {
            id: chart_background1
            x: 0
            y: 0
            source: "../../assets/DashboardContents/performance_background_195_28.png"
        }

        Text {
            id: title_performance
            x: 30
            y: 13
            color: "#5088ab"
            text: "Performance"
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Text {
            id: description_performance
            x: 30
            y: 77
            color: "#bdbebf"
            text: "Q/A fail / pass (last hour)"
            font.family: "Titillium Web"
            font.pixelSize: 28
        }

        Image {
            id: bar_chart
            x: 35
            y: 140
            source: "../../assets/DashboardContents/bar_chart_195_557.png"
        }
    }

    Item {
        id: availability
        x: 699
        y: 30
        Image {
            id: chart_background2
            x: 0
            y: 0
            source: "../../assets/DashboardContents/performance_background_195_28.png"
        }

        Text {
            id: title_availability
            x: 30
            y: 13
            color: "#5088AB"
            text: "Availability"
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Text {
            id: description_availability
            x: 30
            y: 77
            color: "#BDBEBF"
            text: "Machine state (last hour)"
            font.family: "Titillium Web"
            font.pixelSize: 28
        }

        Item {
            id: pie_chart1
            x: 132
            y: 145
            Image {
                id: pie_chart_195_511Asset
                x: 0
                y: 0
                source: "../../assets/DashboardContents/pie_chart_195_511.png"
            }

            Text {
                id: captions_availability
                x: 340
                y: -7
                width: 135
                height: 136
                color: "#BDBEBF"
                text: "Producing\nCalibrating\nEmergency\nOff"
                font.capitalization: Font.AllUppercase
                lineHeight: 1.15
                wrapMode: Text.WordWrap
                font.family: "Titillium Web"
                font.pixelSize: 20
            }
        }
    }

    Item {
        id: quality
        x: 33
        y: 424
        Image {
            id: chart_background3
            x: 0
            y: 0
            source: "../../assets/DashboardContents/performance_background_195_28.png"
        }

        Text {
            id: title_quality
            x: 30
            y: 13
            color: "#5088AB"
            text: "Quality"
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Text {
            id: description_quality
            x: 30
            y: 77
            color: "#BDBEBF"
            text: "Product quality (last hour)"
            font.family: "Titillium Web"
            font.pixelSize: 28
        }

        Item {
            id: pie_chart2
            x: 132
            y: 145
            Image {
                id: pie_chart_195_518Asset
                x: 0
                y: 0
                source: "../../assets/DashboardContents/pie_chart_195_518.png"
            }

            Text {
                id: captions_quality
                x: 340
                y: -7
                width: 135
                height: 107
                color: "#BDBEBF"
                text: "Good
Bad
Review"
                font.capitalization: Font.AllUppercase
                lineHeight: 1.15
                wrapMode: Text.WordWrap
                font.family: "Titillium Web"
                font.pixelSize: 20
            }
        }
    }

    Item {
        id: electricity_195_237
        x: 699
        y: 424
        Image {
            id: chart_background4
            x: 0
            y: 0
            source: "../../assets/DashboardContents/performance_background_195_28.png"
        }

        Text {
            id: title_electricity
            x: 30
            y: 13
            color: "#5088AB"
            text: "Electricity"
            font.family: "Titillium Web"
            font.pixelSize: 40
        }

        Text {
            id: description_electricity
            x: 30
            y: 77
            color: "#BDBEBF"
            text: "Machine power metrics"
            font.family: "Titillium Web"
            font.pixelSize: 28
        }

        Item {
            id: line_chart
            x: 33
            y: 139
            Image {
                id: line_chart_195_513Asset
                x: 0
                y: 0
                source: "../../assets/DashboardContents/line_chart_195_513.png"
            }

            Text {
                id: captions_electricity
                x: 439
                y: -2
                width: 135
                color: "#BDBEBF"
                text: "Current (A)
Power (W)"
                lineHeight: 1.15
                font.capitalization: Font.AllUppercase
                wrapMode: Text.WordWrap
                font.family: "Titillium Web"
                font.pixelSize: 20
            }
        }
    }
}
