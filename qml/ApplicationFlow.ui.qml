import QtQuick 2.10
import dgautomation 1.0
import "BottomNav"
import "StatusContents"
import "LoggingContents"
import "DashboardContents"
import "MaintenanceContents"
import "AboutContents"

Rectangle {
    id: applicationFlow
    width: Constants.width
    height: Constants.height
    color: Constants.backgroundColor

    StatusContents {
        id: statusContents
        anchors.horizontalCenter: parent.horizontalCenter
    }
    LoggingContents {
        id: loggingContents
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
    }
    DashboardContents {
        id: dashboardContents
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
    }
    MaintenanceContents {
        id: maintenanceContents
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
    }
    AboutContents {
        id: aboutContents
        visible: false
        anchors.horizontalCenter: parent.horizontalCenter
    }

    BottomNav {
        id: bottomNav
        x: 36
        y: 786
    }

    states: [
        State {
            name: "StatusContents"
            PropertyChanges {
                target: statusContents
                visible: true
            }
            PropertyChanges {
                target: loggingContents
                visible: false
            }
            PropertyChanges {
                target: dashboardContents
                visible: false
            }
            PropertyChanges {
                target: maintenanceContents
                visible: false
            }
            PropertyChanges {
                target: aboutContents
                visible: false
            }
        },
        State {
            name: "LoggingContents"
            PropertyChanges {
                target: statusContents
                visible: false
            }
            PropertyChanges {
                target: loggingContents
                visible: true
            }
            PropertyChanges {
                target: dashboardContents
                visible: false
            }
            PropertyChanges {
                target: maintenanceContents
                visible: false
            }
            PropertyChanges {
                target: aboutContents
                visible: false
            }
        },
        State {
            name: "DashboardContents"
            PropertyChanges {
                target: statusContents
                visible: false
            }
            PropertyChanges {
                target: loggingContents
                visible: false
            }
            PropertyChanges {
                target: dashboardContents
                visible: true
            }
            PropertyChanges {
                target: maintenanceContents
                visible: false
            }
            PropertyChanges {
                target: aboutContents
                visible: false
            }
        },
        State {
            name: "MaintenanceContents"
            PropertyChanges {
                target: statusContents
                visible: false
            }
            PropertyChanges {
                target: loggingContents
                visible: false
            }
            PropertyChanges {
                target: dashboardContents
                visible: false
            }
            PropertyChanges {
                target: maintenanceContents
                visible: true
            }
            PropertyChanges {
                target: aboutContents
                visible: false
            }
        },
        State {
            name: "AboutContents"
            PropertyChanges {
                target: statusContents
                visible: false
            }
            PropertyChanges {
                target: loggingContents
                visible: false
            }
            PropertyChanges {
                target: dashboardContents
                visible: false
            }
            PropertyChanges {
                target: maintenanceContents
                visible: false
            }
            PropertyChanges {
                target: aboutContents
                visible: true
            }
        }
    ]

    Connections {
        target: bottomNav.btn_Status
        onClicked: {
            applicationFlow.state = "StatusContents"
        }
    }
    Connections {
        target: bottomNav.btn_Logging
        onClicked: {
            applicationFlow.state = "LoggingContents"
        }
    }
    Connections {
        target: bottomNav.btn_Dashboard
        onClicked: {
            applicationFlow.state = "DashboardContents"
        }
    }
    Connections {
        target: bottomNav.btn_Maintenance
        onClicked: {
            applicationFlow.state = "MaintenanceContents"
        }
    }
    Connections {
        target: bottomNav.btn_About
        onClicked: {
            applicationFlow.state = "AboutContents"
        }
    }
}
